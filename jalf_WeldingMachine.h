#ifndef __JALF_WELDINGMACHINE_H__
#define __JALF_WELDINGMACHINE_H__

#include <Arduino.h>
#include <EEPROM.h>
#include "jalf_EEPROM.h"
#include "jalf_Display.h"
#include "jalf_pin_definitions.h"

#define STATE_IDLE          0
#define STATE_SQUEEZE       1
#define STATE_PREHEAT       2
#define STATE_WAIT          3
#define STATE_WELD          4
#define STATE_FORGE         5

class jalf_WeldingMachine{
  /** 
   * \brief The class for the welding machine itself
   */
  private:
  bool invert;
  uint8_t on,off;
  uint16_t    preheat_time;     //= 50;
  uint16_t    wait_time = 400; 
  uint16_t    weld_time ;       //= 100;
  uint16_t    forge_time = 200;
  
  public:
  uint8_t transformer_pin;
  uint8_t solenoid_pin;

  uint8_t     current_state;
  uint16_t    milliseconds;
  uint16_t    squeeze_time = 100;

  jalf_WeldingMachine(uint8_t transformer_pin, uint8_t solenoid_pin,bool invert);
  void weld(jalf_Display* display);
  void update(jalf_Display* display);
  void set_invert(bool val);
  bool get_invert();

  void set_weld_time(uint16_t value);
  uint16_t get_weld_time();
  void set_preheat_time(uint8_t value);
  uint8_t get_preheat_time();
};

#endif //ifndef __JALF_WELDINGMACHINE_H__
