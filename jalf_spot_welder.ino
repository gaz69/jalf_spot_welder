#include "jalf_spot_welder.h"
#define THREE_SECONDS 3000
#define ONE_SECOND 1000

volatile jalf_ButtonPad button_pad;//=jalf_ButtonPad(BUTTONS_INTERRUPT,BUTTON_SELECT,BUTTON_UP,BUTTON_DOWN,BUTTON_CANCEL,BUTTON_WELD);
volatile jalf_WeldingMachine* welder;
volatile jalf_ControlPanel* control_panel;
volatile jalf_Display_Lcd* display;
volatile LiquidCrystal* lcd;

bool button_press = false;

void setup() {
  
  button_pad = jalf_ButtonPad(BUTTONS_INTERRUPT,BUTTON_SELECT,BUTTON_UP,BUTTON_DOWN,BUTTON_CANCEL,BUTTON_WELD);
  welder = new jalf_WeldingMachine(PIN_TRANSFORMER,PIN_SOLENOID,false);
  lcd = new LiquidCrystal(LCD_RS,LCD_ENABLE,LCD_D4,LCD_D5,LCD_D6,LCD_D7);  
  display = new jalf_Display_Lcd(lcd);
  control_panel = new jalf_ControlPanel(welder,&button_pad,display);

  // init timer
  cli();                    // disable interrupts

  //init timer registers
  TCNT2 = 0;
  TCCR2A = 0;
  TCCR2B = 0;
  TIMSK2 = 0;

  TCCR2A |= (1 << WGM21);   // CTC mode
  TCCR2B |= (1 << CS22);    // prescaler 1/64 -> 16000000 / 64 = 250000
  
  OCR2A = 250;              // 1 ms
  TIMSK2 |= (1 << OCIE2A);  // enable output compare interrupt for OCR2A
  
  sei();                    // enable interrupts

  attachInterrupt(digitalPinToInterrupt(button_pad.interrupt_pin),control_panel_buttons_interrupt,FALLING);

  //wait three seconds before starting
  while(millis() < ONE_SECOND){
    ;
  }
  
  Serial.begin(9600);
  Serial.println("jalf_spot_welder 2021-May-02");
  Serial.println("https://gitlab.com/gaz69/jalf_spot_welder");
  Serial.println("git-branch: feature-new-control-architechture");
  Serial.println("WIP: Yes");
  
  
  display->clear();
  display->print("READY");
  button_press = false;
}

void loop() {
  control_panel->update(button_pad.check());
  
  welder->update(display);

  if (button_press){
    on_button_press();
  }
  
  
}

ISR(TIMER2_COMPA_vect) {
//  g_milliseconds ++;
  switch (welder->current_state){
    case STATE_IDLE:
      return;
    default:
      if (welder->milliseconds > 0){
        welder->milliseconds --;
      }
      return;
  }
}

void on_button_press(){
  if (button_pad.debouncing){
    return;
  }
  if (button_pad.button_select.read() == LOW){
    button_pad.active_button = &button_pad.button_select;
  }
  else if (button_pad.button_cancel.read() == LOW){
    button_pad.active_button = &button_pad.button_cancel;
  }
  else if (button_pad.button_up.read() == LOW){
    button_pad.active_button = &button_pad.button_up;
  }
  else if (button_pad.button_down.read() == LOW){
    button_pad.active_button = &button_pad.button_down;
  }
  else if (button_pad.button_weld.read() == LOW){
    button_pad.active_button = &button_pad.button_weld;
  }
  else{    
  /*  Serial.println("Error: unknown button pressed!");
    button_press = false;
    */
    return;
  }
  
  button_pad.active_button->state = ButtonState::DEBOUNCING;
  button_pad.button = button_pad.active_button->pin;  
  button_pad.last_value_read = LOW;
  button_pad.end_debounce_time = millis() + button_pad.debounce_time;
  button_pad.debouncing = true;

  button_press = false;

  Serial.println(button_pad.active_button->name);
}

void control_panel_buttons_interrupt(){
  button_press = true;
}
