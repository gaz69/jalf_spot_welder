#ifndef __JALF_EEPROM_H__
#define __JALF_EEPROM_H__

#define EEPROM_CONTRAST       0
#define EEPROM_WELD_TIME_LOW  1
#define EEPROM_WELD_TIME_HIGH 2
#define EEPROM_PREHEAT_TIME   3

#endif //ifndef __JALF_EEPROM_H__
