#ifndef __JALF_PIN_DEFINITIONS_H__
#define __JALF_PIN_DEFINITIONS_H__

#define PIN_CONTRAST        9   // PWM for Contrast
#define BUTTONS_INTERRUPT   2   // this is the hardware interrupt pin for the buttons panel,
                                // .. panel buttons will be logically or-ed to this
                                // .. pin to detect button presses. See jalf_Buttons
#define BUTTON_SELECT       4   // 
#define BUTTON_CANCEL       5   // 
#define BUTTON_UP           6   //
#define BUTTON_DOWN         7   //
#define BUTTON_WELD         8   //

#define PIN_TRANSFORMER     13   // output for main welding transformer
#define PIN_SOLENOID        12   // output pin for the holding solenoid (if present)

#define LCD_RS              14 
#define LCD_ENABLE          15 
#define LCD_D4              16
#define LCD_D5              17
#define LCD_D6              18
#define LCD_D7              19

#endif//#ifndef __JALF_PIN_DEFINITIONS_H__
