#include "jalf_WeldingMachine.h"

jalf_WeldingMachine::jalf_WeldingMachine(/*uint8_t interrupt_pin,*/
                                         uint8_t transformer_pin,
                                         uint8_t solenoid_pin,
                                         bool invert){
  uint32_t time;
  
  this->transformer_pin = transformer_pin;
  this->solenoid_pin = solenoid_pin;
  this->set_invert (invert);

  digitalWrite(this->transformer_pin,this->off);
  digitalWrite(this->solenoid_pin,this->off);

  this->current_state = STATE_IDLE;
  this->weld_time = this->get_weld_time();
  this->preheat_time = this->get_preheat_time();

  pinMode(this->transformer_pin,OUTPUT);
  pinMode(this->solenoid_pin,OUTPUT);  

  digitalWrite(this->transformer_pin,this->off);
  digitalWrite(this->solenoid_pin,this->off);
}

void jalf_WeldingMachine::weld(jalf_Display* display) {
  Serial.print(this->current_state);
  if (this->current_state == STATE_IDLE){
    this->current_state = STATE_SQUEEZE;
    this->milliseconds = this->squeeze_time;
    digitalWrite(this->solenoid_pin,this->on);

    display->clear();
    display->print("WELDING");

    Serial.println("weld!");
    Serial.print ("welder state: ");
    Serial.println(this->current_state);

  }
}

void jalf_WeldingMachine::update(jalf_Display* display){
  if (this->current_state != STATE_IDLE){
    if (this->milliseconds == 0){
      switch (this->current_state){
        case STATE_SQUEEZE:
          this->current_state = STATE_PREHEAT;
          this->milliseconds = this->preheat_time;
          digitalWrite(this->transformer_pin,this->on);
          break;
        case STATE_PREHEAT:
          this->current_state = STATE_WAIT;
          this->milliseconds = this->wait_time;
          digitalWrite(this->transformer_pin,this->off);
          break;
        case STATE_WAIT:
          this->current_state = STATE_WELD;
          this->milliseconds = this->weld_time;
          digitalWrite(this->transformer_pin,this->on);
          break;
        case STATE_WELD:
          this->current_state = STATE_FORGE;
          this->milliseconds = this->forge_time;
          digitalWrite(this->transformer_pin,this->off);
          break;
        case STATE_FORGE:
          this->current_state = STATE_IDLE;
          this->milliseconds = 0;
          digitalWrite(this->transformer_pin,this->off);
          digitalWrite(this->solenoid_pin,this->off);
          display->clear();
          display->print("READY");
          break;
        default:
          break;   
      }
      Serial.print ("welder state: ");
      Serial.println(this->current_state);
    }
  }
}

void jalf_WeldingMachine::set_invert(bool val){
  this->invert = val;
  if (this->invert){
    this->off = HIGH;
    this->on = LOW;
  }else{
    this->off = LOW;
    this->on = HIGH;
  }
}

bool jalf_WeldingMachine::get_invert(){
  return this->invert;
}

void jalf_WeldingMachine::set_weld_time(uint16_t value)
{
  this->weld_time = value;
  EEPROM.write(EEPROM_WELD_TIME_LOW,value & 0x00FF);
  EEPROM.write(EEPROM_WELD_TIME_HIGH,value >> 8);
  Serial.print("set_weld_time: LOW = ");
  Serial.println(EEPROM.read(EEPROM_WELD_TIME_LOW));
  Serial.print("set_weld_time: HIGH = ");
  Serial.println(EEPROM.read(EEPROM_WELD_TIME_HIGH));
  Serial.print("set_weld_time: this-> weld_time = ");
  Serial.println(this->weld_time);
}
uint16_t jalf_WeldingMachine::get_weld_time()
{
  this->weld_time = EEPROM.read(EEPROM_WELD_TIME_LOW);
  this->weld_time |= EEPROM.read(EEPROM_WELD_TIME_HIGH) << 8 ;
  return this->weld_time;
}
void jalf_WeldingMachine::set_preheat_time(uint8_t value)
{
  this->preheat_time = value;
  EEPROM.write(EEPROM_PREHEAT_TIME,value);
}
uint8_t jalf_WeldingMachine::get_preheat_time()
{
  this->preheat_time = EEPROM.read(EEPROM_PREHEAT_TIME);
  return this->preheat_time;
}
