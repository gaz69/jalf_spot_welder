#ifndef __JALF_CONTROLPANEL_H__
#define __JALF_CONTROLPANEL_H__

#include <Arduino.h>
#include <LiquidCrystal.h>
#include "jalf_WeldingMachine.h"
#include "jalf_ButtonPad.h"
#include "jalf_Display.h"
#include "jalf_pin_definitions.h"
#include "jalf_Selection.h"

#define SELECT_WELD_TIME        0
#define SELECT_PREHEAT_TIME     1
#define SELECT_DEBOUNCE_TIME    2
#define SELECT_CONTRAST         3

class jalf_ControlPanel{
  /** 
   *  \brief A class wich models a control panel with a button pad and a display.
   *  It is not realy generic because it muss hava knowledge of the machine being controlled :(
   */
  private:
   
  uint8_t select;

  jalf_Selection current_selection;

  jalf_Selection selection_weld_time;
  jalf_Selection selection_preheat_time;
  jalf_Selection selection_debounce_time;
  jalf_Selection selection_contrast;
  
  jalf_WeldingMachine* welder;
  jalf_ButtonPad* button_pad;
  jalf_Display* display;

  void init(jalf_WeldingMachine* welder,jalf_ButtonPad* button_pad);
  
  public:
  jalf_ControlPanel(jalf_WeldingMachine* welder,jalf_ButtonPad* button_pad);
  jalf_ControlPanel(jalf_WeldingMachine* welder,jalf_ButtonPad* button_pad,jalf_Display* display);
  jalf_ControlPanel(jalf_WeldingMachine* welder,jalf_ButtonPad* buttons_pad,LiquidCrystal* lcd);
//  uint8_t update(uint8_t button);
  uint8_t update(jalf_Button* button);
  jalf_Display* get_display_ptr();
};

#endif //ifndef __JALF_CONTROLPANEL_H__
