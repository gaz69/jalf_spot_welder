#ifndef __JALF_BUTTON_H__
#define __JALF_BUTTON_H__

#include <Arduino.h>

enum ButtonState {IDLE = 0,PRESSED = 1,DEBOUNCING = 2};

class jalf_Button{
/**
 * \brief Push-Button class
 */
  public:

  
  uint8_t pin;
  char* name;
  char* text;

  ButtonState state;
  
  jalf_Button();
  jalf_Button(uint8_t pin,char* name,char* text);
  uint8_t read();
  void debug();
};

#endif//ifndef __JALF_BUTTON_H
