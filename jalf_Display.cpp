#include "jalf_Display.h"

jalf_Display_Lcd::jalf_Display_Lcd(LiquidCrystal* lcd){
  this->lcd = lcd;
}
void jalf_Display_Lcd::init()
{
  lcd->begin(16,2);
  pinMode(PIN_CONTRAST,OUTPUT);
  analogWrite(PIN_CONTRAST,this->get_contrast());
  this->clear();
}

uint8_t jalf_Display_Lcd::get_contrast(){
  this->contrast = EEPROM.read(EEPROM_CONTRAST);
  if (this->contrast == 0xFF | this->contrast == 0){
    this->contrast = 65;
  }
  return this->contrast;
}

void jalf_Display_Lcd::set_contrast(uint8_t value){
  this->contrast = value;
  analogWrite(PIN_CONTRAST,this->contrast);
  EEPROM.write(EEPROM_CONTRAST,this->contrast);
}


void jalf_Display_Lcd::print(uint16_t value){
  this->lcd->print(value);
}
void jalf_Display_Lcd::print(int16_t value){
  this->lcd->print(value);
}
void jalf_Display_Lcd::print(int32_t value){
  this->lcd->print(value);
}
void jalf_Display_Lcd::print(float value){
  this->lcd->print(value);
}
void jalf_Display_Lcd::print(char character){
  this->lcd->print(character);
}

void jalf_Display_Lcd::print(char* str){
  this->lcd->print(str);
}

void jalf_Display_Lcd::print(const char* &str){
  this->lcd->print(str);
}

void jalf_Display_Lcd::setCursor(uint8_t column,uint8_t line){
  this->lcd->setCursor(column,line);
}

void jalf_Display_Lcd::clear(){
  this->lcd->clear();
}
