#include "jalf_Button.h"

  jalf_Button::jalf_Button(void){}
  
  jalf_Button::jalf_Button(uint8_t pin,char* name,char* text){
    
    this->pin = pin;
    this->name = name;
    this->text = text;
    pinMode(this->pin,INPUT_PULLUP);
    this->state = ButtonState::IDLE;
    this->debug();
  }

  void jalf_Button::debug(){
    Serial.print("button ");
    Serial.print(this->name);
    Serial.print(" on pin ");
    Serial.println(this->pin);
  }
  
  uint8_t jalf_Button::read(){
    int val = digitalRead(this->pin);
    return val;
  }
