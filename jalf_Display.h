#ifndef __JALF_DISPLAY_H__
#define __JALF_DISPLAY_H__

#include <Arduino.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>
#include "jalf_EEPROM.h"
#include "jalf_pin_definitions.h"

class jalf_Display{
  /**
   * Generic (virtual) class for defining Displays
   */
  private:

  protected:
  uint8_t contrast; // = 65;

  public:
  
  virtual void init();
  virtual void print(uint16_t value);
  virtual void print(int16_t)=0;
  virtual void print(int32_t)=0;
  virtual void print(float)=0;
  virtual void print(char)=0;
  virtual void print(char*)=0;
  virtual void print(const char*&)=0;
  virtual void clear();
  virtual void setCursor(uint8_t column, uint8_t line);

  virtual uint8_t get_contrast(void);
  virtual void set_contrast(uint8_t value);
  
};

class jalf_Display_Lcd:public jalf_Display{
  /**
   * Specific implementation of the generic Display class by using an LCD
   */
  private:
  LiquidCrystal* lcd;

  
  public:
  jalf_Display_Lcd(LiquidCrystal* lcd);
  void init();
  void print(uint16_t value);
  void print(int16_t);
  void print(int32_t);
  void print(float);
  void print(char);
  void print(char*);
  void print(const char*&);
  void clear ();
  void setCursor(uint8_t column,uint8_t line);
  
  uint8_t get_contrast(void);
  void set_contrast(uint8_t value);

};
#endif //#ifndef __JALF_DISPLAY_H__
