#include "jalf_ButtonPad.h"

jalf_ButtonPad::jalf_ButtonPad(void){}

jalf_ButtonPad::jalf_ButtonPad(uint8_t interrupt_pin,uint8_t select_pin,uint8_t up_pin,uint8_t down_pin,uint8_t cancel_pin,uint8_t weld_pin){
  uint8_t suc;
  this->interrupt_pin = interrupt_pin;
  this->select = select_pin;  //new jalf_Button(select_pin,jalf_Button::Function::SELECT);
  this->up = up_pin;          //new jalf_Button(up_pin,jalf_Button::Function::UP);
  this->down = down_pin;      //new jalf_Button(down_pin,jalf_Button::Function::DOWN);
  this->cancel = cancel_pin;  //new jalf_Button(cancel_pin,jalf_Button::Function::CANCEL);
  this->weld = weld_pin;      //new jalf_Button(weld_pin,jalf_Button::Function::WELD);
  this->button = NULL;
  this->debouncing=false;

  
  this->button_select = jalf_Button(select_pin,"select","Select");
  this->button_up = jalf_Button(up_pin,"up","Up");
  this->button_down = jalf_Button(down_pin,"down","Down");
  this->button_cancel =jalf_Button(cancel_pin,"cancel","Cancel");
  this->button_weld = jalf_Button(weld_pin,"weld","Weld");
  
  this->active_button = NULL;
      
  pinMode(this->interrupt_pin,INPUT_PULLUP);
  this->clear();
}

uint8_t jalf_ButtonPad::get_debounce_time(){
  return this->debounce_time;
}

void jalf_ButtonPad::set_debounce_time(uint8_t milliseconds){
  this->debounce_time = milliseconds;
}

jalf_Button* jalf_ButtonPad::check(){
/**
 * \brief Checks the debouncing state of buttons.
 */
  jalf_Button* result = NULL;

  if (this->debouncing){
    if (millis() > this->end_debounce_time &&                     // debounce time expired
        this->active_button->read() == HIGH){                     // and button is depressed
      this->debouncing = false;                                   //   stop debouncing
      result = this->active_button;                               //   this button is now debounced!
    }else{                                                        // debounce time NOT expired
      if (this->active_button->read() != this->last_value_read){  //   change of state (bounce)
        this->end_debounce_time = millis() + this->debounce_time; //     restart the timer
        this->last_value_read = this->active_button->read();      //     save the reading
      }else if (this->active_button->read() != HIGH){             // no change but button still pressed
        this->end_debounce_time = millis() + this->debounce_time; //   restart the timer
      }
    }

  }

  return result;                                                   
}

void jalf_ButtonPad::clear(){
  this->debouncing = false;
  this->button = 0;
  this->active_button->state = ButtonState::IDLE;
  this->active_button = NULL;
}
