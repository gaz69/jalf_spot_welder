# Arduino controller for a homemade resistance (spot) welding machine.

## The machine consists of: 
* a control panel with an Lcd display as well as three buttons
  * Selection
  * Up
  * Down
* a "weld" button
* a solid state relay for the control of the power to the transformer
* a transformer

## Button interrupts
* Button presses are sensed using harware interrupt on pin 2
* A diode-logic AND gate senses button presses. 
* For more detailed information see file *schematics.txt*

## Classes:
Each class consists of a .h and a .cpp file
* jalf_ControlPanel
* jalf_Selection
* jalf_ButtonPad
* jalf_Button
* jalf_Display
* jalf_Display_Lcd
* jalf_WeldingMachine

## Misc:
* jalf_pin_definitions.h
* jalf_EEPROM.h
* schematics.txt


