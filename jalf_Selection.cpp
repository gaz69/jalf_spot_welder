#include <Arduino.h>
#include "jalf_Selection.h"

jalf_Selection::jalf_Selection(){}

jalf_Selection::jalf_Selection(char* text,char* name,uint8_t index)
{
  this->name = name;
  this->text = text;  
  this->index = index;
}

jalf_Selection::jalf_Selection(char* name,char*text){
  this->text = text;
  this->name = name;
}

void jalf_Selection::print(jalf_Display* display){
  uint8_t position_value = 13;
  display->setCursor(0,0);
  display->print("                ");
  display->setCursor(0,0);
  display->print(this->text);
  
  if (this->data > 999){
    position_value --;
  }
  if (this->data > 9999){
    position_value --;
  }
  if (this->data > 99999){
    position_value --;
  }
  display->setCursor(position_value,0);
  display->print(this->data);
//  Serial.print(this->name);
//  Serial.print(this->text);
//  Serial.print(" ");
//  Serial.println(this->data);
}
