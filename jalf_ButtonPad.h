#ifndef __BUTTONS_H__
#define __BUTTONS_H__

#include <Arduino.h>
#include "jalf_Button.h"
#include "jalf_Display.h"

#define MAX_BUTTONS     5

class jalf_ButtonPad{
/**
 * \brief A class for a pad of buttons. It takes care of checking which button was pressed and of debouncing. 
 *        The actual detection of button presses happens in an Interrupt Service Routine. See schematics.txt 
 *        for a more detailed explanation.
 *        IMPORTANT: It only keeps track of one button press at a time. 
 *        This means that the last button pressed overrides previously button-press events and there are no handling for 
 *        pressing two buttons simultaneously
 */
  private:
    
  public:
    volatile uint8_t button;
    jalf_Button* active_button = NULL; 

    uint8_t debounce_time = 20;
    volatile bool debouncing;
    volatile uint8_t last_value_read;
    volatile uint32_t end_debounce_time;
    
    uint8_t interrupt_pin;
    uint8_t select;
    uint8_t up;
    uint8_t down;
    uint8_t cancel;
    uint8_t weld;

    jalf_Button button_select;
    jalf_Button button_up;
    jalf_Button button_down;
    jalf_Button button_cancel;
    jalf_Button button_weld;
    
    jalf_ButtonPad();
    jalf_ButtonPad(uint8_t interrupt_pin,uint8_t select,uint8_t up,uint8_t down,uint8_t cancel,uint8_t weld);
    
    jalf_Button* check();

    void set_debounce_time(uint8_t milliseconds);
    uint8_t get_debounce_time();
    void clear();
    
};

#endif //#ifndef __BUTTONS_H__
