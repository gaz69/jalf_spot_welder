#include "jalf_ControlPanel.h"

jalf_ControlPanel::jalf_ControlPanel(jalf_WeldingMachine* welder,jalf_ButtonPad* buttons,jalf_Display* display){
  this->init(welder,buttons);
  this->display = display;
  this->display->init();
}

jalf_ControlPanel::jalf_ControlPanel(jalf_WeldingMachine* welder,jalf_ButtonPad* button_pad){
  this->init(welder,button_pad);
}

void jalf_ControlPanel::init(jalf_WeldingMachine* welder,jalf_ButtonPad* button_pad){
/**  
 \brief Initializes the control panel, should allways be call by the constructor
 */

  selection_weld_time = jalf_Selection("WELD TIME","Weld time",0);
  selection_preheat_time = jalf_Selection("PREHEAT TIME","Preheat time",1),
  selection_debounce_time = jalf_Selection("DEBOUNCE","Debounce",2),
  selection_contrast = jalf_Selection("CONTRAST","Contrast",3);

  this->current_selection = selection_contrast;
  this->select = SELECT_CONTRAST;
  this->welder = welder;
  this->button_pad = button_pad;
}

uint8_t jalf_ControlPanel::update(jalf_Button* button){
  /**
  \brief Updates the control panel after a button press
  */
  uint16_t value;
  uint8_t contrast;
   
  if (button == NULL){
    return;
  }
  
  switch(button->pin){
    case BUTTON_SELECT:
      switch (this->select){
        case SELECT_WELD_TIME:
          this->select = SELECT_PREHEAT_TIME;
          value = this->welder->get_preheat_time();
          current_selection = selection_preheat_time;
          break;
        case SELECT_PREHEAT_TIME:
          this->select = SELECT_DEBOUNCE_TIME;
          value = this->button_pad->debounce_time;
          current_selection = selection_debounce_time;
          break;
        case SELECT_DEBOUNCE_TIME:
          this->select = SELECT_CONTRAST;
          value = this->display->get_contrast();
          current_selection = selection_contrast;
          break;
        case SELECT_CONTRAST:
          this->select = SELECT_WELD_TIME;
          value = this->welder->get_weld_time();
          current_selection = selection_weld_time;
          break;
      }
      current_selection.data = value;
      current_selection.print(this->display);
      /*this->selection[this->select].data = value;
      this->selection[this->select].print(this->display);*/
      break;
    case BUTTON_UP:
      switch (this->select){
        case SELECT_WELD_TIME:
          value = this->welder->get_weld_time();
          if (value >= 5000) {
            value += 500;
          }
          else if (value >= 1000){
            value += 100;
            if (value > 5000) value=5000;
          }
          else if (value >= 500){
            value += 50;
            if (value > 1000) value=1000;
          }
          else if (value >= 100){
            value += 10;
            if (value > 500) value = 500;
          }
          else{
            value ++;
          }
          this->welder->set_weld_time(value);
          break;
        case SELECT_PREHEAT_TIME:
          value = this->welder->get_preheat_time() + 1;
          this->welder->set_preheat_time(value);
          break;
        case SELECT_DEBOUNCE_TIME:
          value = this->button_pad->get_debounce_time() + 1;
          this->button_pad->set_debounce_time(value);
          break ;
        case SELECT_CONTRAST:
          value = this->display->get_contrast() + 1;
          this->display->set_contrast(value);
          break ;
      }
      current_selection.data = value;
      current_selection.print(this->display);
      /*this->selection[this->select].data = value;
      this->selection[this->select].print(this->display);*/
      break;
    case BUTTON_DOWN:
      switch (this->select){
        case SELECT_WELD_TIME:
          value = this->welder->get_weld_time();
          if (value > 5000){
            value -= 500;
            if (value < 5000) value = 5000;
          }
          else if (value > 1000){ 
            value -= 100;
            if (value < 1000)value = 1000;
          }
          else if (value > 500){
            value -= 50;
            if (value < 100) value = 100;
          }
          else if (value > 100){
            value -= 10;
            if (value < 100) value = 100;
          }
          else{
            value --;
          }
          this->welder->set_weld_time(value);
          break;
        case SELECT_PREHEAT_TIME:
          value = this->welder->get_preheat_time() - 1;
          this->welder->set_preheat_time(value);
                    break;
        case SELECT_DEBOUNCE_TIME:
          value = this->button_pad->get_debounce_time() - 1;
          this->button_pad->set_debounce_time(value);
          break ;
        case SELECT_CONTRAST:
          value = this->display->get_contrast() - 1;
          this->display->set_contrast(value);
          break ;
        }
      current_selection.data = value;
      current_selection.print(this->display);
      /*this->selection[this->select].data = value;
      this->selection[this->select].print(this->display);*/
      break;
    case BUTTON_CANCEL:
      switch (this->select){
        case SELECT_WELD_TIME:
          //IMPORTANT: this case has no break, instead it falls through to next case!
        case SELECT_PREHEAT_TIME:
          this->select = SELECT_WELD_TIME;
          current_selection = selection_weld_time;
          value = this->welder->get_weld_time();
          break;
        case SELECT_DEBOUNCE_TIME:
          this->select = SELECT_PREHEAT_TIME;
          current_selection = selection_preheat_time;
          value = this->welder->get_preheat_time();
          break;
       }
      current_selection.data = value;
      current_selection.print(this->display);
      /*this->selection[this->select].data = value;
      this->selection[this->select].print(this->display);*/
      break;
    case BUTTON_WELD:
      this->welder->weld(this->display);
      //Serial.println("Weld");
      break;    
    default:
      Serial.print("received ");Serial.println(button->name);
      break;
  }
  this->button_pad->clear();
}
