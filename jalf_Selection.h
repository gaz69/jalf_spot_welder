#ifndef __JALF_SELECTION_H__
#define __JALF_SELECTION_H__
#include "jalf_Display.h"

class jalf_Selection{

  private:
  
  public:
    char* text;
    char* name;
    uint8_t index;
    uint16_t data;
    
    
    jalf_Selection();
    jalf_Selection(char* name,char* text,uint8_t index);
    jalf_Selection(char* name,char* text);
    void print(jalf_Display* display);
    
    
};
#endif  //ifndef __JALF_SELECTION_H__
